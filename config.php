<?php 

require __DIR__ . '/vendor/autoload.php'; // charge tout ce qui est installé avec composer

$SITE_URI = "forty";

$loader = new \Twig\Loader\FilesystemLoader($SITE_URI.'/view', getcwd().'/..'); // source (URI) des templates
$twig = new \Twig\Environment($loader);

echo $twig->render('index.html', $index);

?> 

